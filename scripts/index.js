import { Modal } from './components/classes.js';
import { getToken } from './components/requests.js';

const btnEnter = document.querySelector('.btn-enter');

btnEnter.addEventListener('click', () => {
    // const form = new Modal();
    // console.log(form);
    // form.render();

    const modalForm = document.querySelector('.modal');
    const emailForm = modalForm.querySelector('input[type="email"]');
    const passwordForm = modalForm.querySelector('input[type="password"]');
    const btnForm = modalForm.querySelector('.btn-primary');

    function checkInputValue() {
        const values = [];
        [emailForm, passwordForm].forEach((input) => {
            if (!input.value) {
                alert('Ви не ввели всі дані');
                return;
            } else {
                values.push(input.value);
            }
        });
        return values;
    }

    btnForm.addEventListener('click', (e) => {
        e.preventDefault();
        
        const [email, password] = checkInputValue();
        console.log(checkInputValue());
        submitForm(modalForm, email, password);

        document.querySelector('.modal-backdrop').remove();
    });
});

async function submitForm(form, email, password) {
    // const email = 'your@email.com'
    // const password = 'password'
    const token = await getToken(email, password);

    sessionStorage.token = token;

    if (!!token) {
        const createVisitBtn = document.querySelector('.btn-create-visit');
        const filters = document.querySelector('.wrapper-filtering-form');

        createVisitBtn.classList.remove('disappear');
        filters.classList.remove('disappear');
        btnEnter.classList.add('disappear');
        form.style.display = 'none';
        console.log(token);
    }
}
