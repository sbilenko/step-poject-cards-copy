const tokenAPI = 'https://ajax.test-danit.com/api/v2/cards/login'
const cardsAPI = 'https://ajax.test-danit.com/api/v2/cards'

export async function getToken(email, password) {
    const response = await fetch(
        'https://ajax.test-danit.com/api/v2/cards/login',
        {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            }),
        }
    )

    if (response.ok) {
        return response.text()
    } else {
        throw new Error('Такого користувача немає')
    }
}
